const $ = require('gulp-load-plugins')();
const gulp = require('gulp');
const webpackStream = require('webpack-stream');
const webpack = webpackStream.webpack;
const named = require('vinyl-named');
const path = require('path');
const gulplog = require('gulplog');


module.exports = function (options) {
    return function (callback) {

        let firstBuildReady = false;

        function done(err, stats) {
            firstBuildReady = true;

            if (err) {
                return;
            }

            gulplog[stats.hasErrors() ? 'error' : 'info'](stats.toString({
                colors: true
            }))
        }

        let opts = {
            watch: true,
            devtool: global.isDevelopment ? 'cheap-module-inline-source-map' : null,
            watchOptions: {
                aggregateTimeoit: 100
            },
            module: {
                loaders: [{
                    test: /\.js$/,
                    exclude: ['../js/script.min.js', '../js/libs.js'],
                    include: path.resolve(__dirname, options.path),
                    loader: 'babel?presets[]=es2015'
                }]
            },
            plugins: [
                new webpack.NoErrorsPlugin()
            ]
        };

        return gulp.src(options.src)
            .pipe($.plumber({
                errorHandler: $.notify.onError(err => ({
                    title: 'Webpack',
                    message: err.message
                }))
            }))
            .pipe(named())
            .pipe(webpackStream(opts, null, done))
            .pipe($.rename('script.min.js'))
            .pipe($.if(!global.isDevelopment, $.uglify()))
            .pipe(gulp.dest(options.dest)
                .on('data', function () {
                    if (firstBuildReady) {
                        callback();
                    }
                }))
    }
};
