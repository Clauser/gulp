/**
 * сборка скриптов
 */
const $ = require('gulp-load-plugins')();
const gulp = require('gulp');
const combiner = require('stream-combiner2').obj;


module.exports = function (options) {
    return function () {
        return (combiner(
            gulp.src(options.src, {since: gulp.lastRun(options.taskName)}),
            $.if(global.isDevelopment, $.sourcemaps.init()),
            $.remember('js'),
            $.concat('script.min.js'),
            $.if(!global.isDevelopment, $.uglify()),
            $.if(global.isDevelopment, $.sourcemaps.write()),
            gulp.dest(options.dest)
        ).on('error', $.notify.onError()));
    }
};/**
 * Created by MadRobots on 20.04.2017.
 */
