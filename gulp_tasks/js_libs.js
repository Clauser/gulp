/**
 * Сборка библиотек
 */

const $ = require('gulp-load-plugins')();
const gulp = require('gulp');
const combiner = require('stream-combiner2').obj;

module.exports = function (options) {
    return function () {
        return (combiner(
            gulp.src(options.src),
            $.concat('libs.min.js'),
            $.uglify(),
            gulp.dest(options.dest)
        ).on('error', $.notify.onError()));
    }
};
