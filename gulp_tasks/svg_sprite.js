const $ = require('gulp-load-plugins')();
const gulp = require('gulp');
const combiner = require('stream-combiner2').obj;

module.exports = function (options) {
    return function () {
        return combiner(
            gulp.src(options.src),
            $.svgSprite({
                mode: {
                    css: {
                        dest: '../css/',
                        bust: false,
                        sprite: 'sprite.svg',
                        layout: 'horizontal',
                        prefix: '.',
                        dimensions: true,
                        render: {
                            less: {
                                dest: 'sprite.less'
                            }
                        }
,                    }
                }
            }),
            gulp.dest(options.dest)
        );
    }
};