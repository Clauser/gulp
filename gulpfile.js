'use strict';
const gulp = require('gulp');
const remember = require('gulp-remember');
const path = require('path');
const browserSync = require('browser-sync').create();


/**
 * определяет тсип сборки
 * нужно передать параметр NODE_ENV (прописан в package.json)
 * @type {boolean}
 */
global.isDevelopment = process.env.NODE_ENV == 'development' || !process.env.NODE_ENV;
/**
 * функция загрузки тасков
 * @param taskName {string} название таска
 * @param path {string} путь к таску
 * @param options {object} настройки сборки
 */
function lazyRequireTasks(taskName, path, options) {
    options = options || {};
    options.taskName = taskName;
    gulp.task(taskName, (callback) => {
        let task = require(path).call(this, options);
        return task(callback);
    });
}

const paths = {
    js: {
        src: ['./js/classes/site.js', './js/classes/header.js', './js/classes/form.js', './js/classes/main_page.js', './js/classes/checkout.js', './js/classes/content.js', './js/classes/product.js'],
        dest: './js/'
    },
    jsWebpack: {
        src: [`./js/*.js`, '!./js/libs.js', '!./js/script.min.js'],
        path: '../js/',
        dest: `./js/`
    },
    libsJS: {
        src: ['./js/libs/jquery-2.2.4.min.js', './js/libs/**/*.js'],
        dest: './js/'
    },
    less: {
        src: ['./css/*.less'],
        watch: ['./css/**/*.less'],
        dest: './css/'
    },
    cssLibs: {
        src: ['./js/libs/*.css', '!js/libs.js'],
        dest: './js/libs/'
    },
    img: {
        src: ['./images/**/*.{svg,png,jpg,jpeg}', '!./images/icons/'],
        dest: './images/'
    },
    sprite: {
        src: ['./images/icons/*.svg'],
        dest: './images/'
    },
    exclude: ['!node_modules/**/*.*', '!gulpfile.js', '!gulp_tasks/**/*.*', '!**/*.css']
};


//TODO: добавить сборку html
//TODO: перенести все в отдельную директорию
//TODO: _? ускорить сборку картинок(проверка дерикторий remember)

/**
 * LESS
 */
lazyRequireTasks('less', './gulp_tasks/less.js', {
    src: paths.less.src,
    dest: paths.less.dest
});
/**
 * сборка css библиотек
 */
lazyRequireTasks('cssLibs', './gulp_tasks/css.js', {
    src: paths.cssLibs.src,
    dest: paths.cssLibs.dest
});
/**
 * сборка JS
 */
lazyRequireTasks('js', './gulp_tasks/js.js', {
    src: paths.js.src,
    dest: paths.js.dest,
    libs: false
});
lazyRequireTasks('js:webpack', './gulp_tasks/webpack.js', paths.jsWebpack);
/**
 * сборка библиотек
 */
lazyRequireTasks('jsLibs', './gulp_tasks/js.js', {
    src: paths.libsJS.src,
    dest: paths.libsJS.dest,
    libs: true
});
/**
 * минификация изображений
 * IMAGES
 */
lazyRequireTasks('imagemin', './gulp_tasks/imagemin.js', {
    src: paths.img.src,
    dest: paths.img.dest
});
/**
 * Спрайты
 */
lazyRequireTasks('sprite:svg', './gulp_tasks/svg_sprite.js', paths.sprite);
/**
 * удаление папки
 */
lazyRequireTasks('cleanCache', './gulp_tasks/clean.js', {
    path: '../site/bitrix/cache/twig'
});
/**
 * копирование
 */
gulp.task('copy', () => {
    return gulp.src(paths.html.src, {since: gulp.lastRun('copy')})
        .pipe(gulp.dest(paths.html.dest))
});

/**
 * сборка проекта
 */
gulp.task('build', gulp.parallel('cleanCache', 'less', 'cssLibs', 'js:webpack', 'jsLibs', 'imagemin', 'sprite:svg'));
/**
 * отслеживание изменения в файлах
 */
gulp.task('watch', () => {
    gulp.watch(paths.js.src, gulp.series(['js']))
        .on('unlink', function (filepath) {
            remember.forget('js', path.resolve(filepath));
        });
    gulp.watch(paths.libsJS.src, gulp.series(['jsLibs']));
    gulp.watch(paths.less.watch, gulp.series(['less']));
    gulp.watch(paths.img.src, gulp.series(['imagemin']));
    gulp.watch(paths.sprite.src, gulp.series(['sprite:svg']));
    if (global.isDevelopment) {
        gulp.watch('./local/templates/twig/**/*.twig', gulp.series(['cleanCache']));
    }
});

gulp.task('server', function () {
    browserSync.init({
        proxy: 'zavtra.local',
        port: 9000
    });

    browserSync.watch(['./*.html', './css/*.css', './js/script.min.js', './local/templates/twig/**/*.twig', './images/**/*.*']).on('change', browserSync.reload);
    browserSync.stream();
});


/**
 * запуск gulp
 */
gulp.task('default', gulp.series(['build', gulp.parallel('watch', 'server')]));